package com.hit8328.app.easypt.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.NearbyStop;
import com.hit8328.app.easypt.domain.NextService;
import com.hit8328.app.easypt.domain.Record;
import com.hit8328.app.easypt.serivce.PTService;
import com.hit8328.app.easypt.serivce.Standard;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by yichao on 17/11/14.
 */
public class StopDetailActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "StopDetailActivity";
    private NearbyStop nearbyStop;  // Stop Passed By Intent
    private NextService nextService;
    private GoogleMap mMap;
    private Location currLoc;

    private ProgressDialog progressDialog;

    private TextView tvStopName;
    private TextView tvType;
    private TextView tvNextService;
    private TextView tvMinToDepart;
    private TextView tvDesc;
    private ImageView btnPrev;
    private ImageView btnNext;

    private Button btnSave;

    private int currentPage;
    private int totalPage;

    private enum operation {
        NEXT_PAGE,
        PREVIOUS_PAGE,
        INITIAL_PAGE
    }

    private enum color {
        RED,
        GREEN,
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Only show items in the action bar relevant to this screen
        // if the drawer is not showing. Otherwise, let the drawer
        // decide what to show in the action bar.
        getMenuInflater().inflate(R.menu.detail_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_stop_detail);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        // Get the Intent
        Intent intent = getIntent();
        nearbyStop = (NearbyStop) intent.getSerializableExtra("stop");

        currLoc = intent.getBundleExtra("currLoc").getParcelable("currLoc");

        // Initialize UI

        initializeUI();

        Log.i(TAG, "StopDetailActivity, received Stop = " + nearbyStop);

    }

    private void initializeUI() {

        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        tvStopName = (TextView) this.findViewById(R.id.tv_stop_name);
        tvType = (TextView) this.findViewById(R.id.tv_type);
        tvNextService = (TextView) this.findViewById(R.id.tv_next_service);
        tvMinToDepart = (TextView) this.findViewById(R.id.tv_min_depart);
        tvDesc = (TextView) this.findViewById(R.id.tv_dest);

        btnPrev = (ImageView) this.findViewById(R.id.btn_prev);
        btnPrev.setOnClickListener(this);
        btnNext = (ImageView) this.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(this);

        btnSave = (Button) this.findViewById(R.id.save_record);
        btnSave.setOnClickListener(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Getting Information");
        progressDialog.setMessage("Please wait...");
        progressDialog.setIndeterminate(true);

        // Set the Information
        new ObtainServiceTask().execute(nearbyStop);
        tvStopName.setText(nearbyStop.getResult().getLocation_name());
//        tvNextService.setText(tvNextService.getText() + "" + stop.getResult().);

//        // Set Marker
        addMarker(currLoc.getLongitude(), currLoc.getLatitude(), "", color.GREEN);
        addMarker(Double.parseDouble(nearbyStop.getResult().getLon()), Double.parseDouble(nearbyStop.getResult().getLat()), nearbyStop.getResult().getLocation_name(), color.RED);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_prev:
                updatePage(operation.PREVIOUS_PAGE);
                break;
            case R.id.btn_next:
                updatePage(operation.NEXT_PAGE);
                break;
            case R.id.save_record:
                saveRecord();
                break;
        }
    }

    private void saveRecord() {
        // Get Information
        String stopName = nearbyStop.getResult().getLocation_name();
        if (stopName.length() > 0) {

            // Container
            Set<Record> set = null;

            Date timeArrival = convertUTCTime(nextService.getValues()[currentPage - 1].getTime_timetable_utc());
            Record record = new Record(stopName, timeArrival, nextService.getValues()[currentPage - 1].getRun().getDestination_name(),
                    nextService.getValues()[currentPage - 1].getRun().getTransport_type(), Double.parseDouble(nearbyStop.getResult().getLon()),
                    Double.parseDouble(nearbyStop.getResult().getLat()));

            Gson gson = new Gson();

            // Get Preference
            SharedPreferences settings = getSharedPreferences(Standard.PREF_FILE, 0);
            String jsonStr = settings.getString("record", "");   // Json Sting
            if (jsonStr.length() == 0) {
//                set = new HashSet<Record>();
//                set.add(record);
                jsonStr = gson.toJson(new Record[]{record});

                // Store JSON
                settings.edit();
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("record", jsonStr);
                editor.apply();

                Log.i(TAG, "JSON = " + jsonStr);

            } else {
                // Append the record

                // Get the Set
                Record[] records = gson.fromJson(jsonStr, Record[].class);
                Record[] newRecords = new Record[records.length + 1];

                for (int i = 0; i < records.length; i++) {
                    newRecords[i] = records[i];
                }
                newRecords[newRecords.length - 1] = record;

                // Save to Preference
                jsonStr = gson.toJson(newRecords);
                // Store JSON
                settings.edit();
                SharedPreferences.Editor editor = settings.edit();
                editor.putString("record", jsonStr);
                editor.apply();

                Log.i(TAG, "Number: " + newRecords.length + ", saveRecord records = " + newRecords + "JSON = " + jsonStr);


            }

            Toast.makeText(this, "The service has been recorded.", Toast.LENGTH_SHORT).show();

        }
    }

    private class ObtainServiceTask extends AsyncTask<NearbyStop, Void, NextService> {

        @Override
        protected void onPreExecute() {

            progressDialog.show();
        }


        @Override
        protected NextService doInBackground(NearbyStop... nearbyStops) {
//            Log.i(TAG, new PTService().healthCheck() ? "a" : "b");
            nextService = new PTService().getNextService(nearbyStops[0].getResult().getTransport_type(), nearbyStops[0].getResult().getStop_id());

            Log.i(TAG, "doInbackground nextService = " + nextService.getValues().length);


            return nextService;

        }

        @Override
        protected void onPostExecute(NextService nextService) {

            if (nextService == null) {
                totalPage = 0;
            } else {
                totalPage = nextService.getValues().length;

            }
            updatePage(operation.INITIAL_PAGE);

//            if(success){
//                showResult();
//            }else{
//                Toast.makeText(StopDetailActivity.this, "Please Check Internet", Toast.LENGTH_SHORT).show();
//            }
            // Close the Progress Dialog
            progressDialog.dismiss();
        }


    }

    /**
     * Control the visibility of the image button
     *
     * @param initialPage operation
     */
    private void updatePage(operation initialPage) {
        Log.i(TAG, "beforeUpdate: currentPage = " + currentPage + ", totalPage = " + totalPage);
        if (initialPage == operation.INITIAL_PAGE) {
            currentPage = 1;
            // Initialize the page
            if (totalPage <= 1) {
                btnNext.setVisibility(View.INVISIBLE);
                btnPrev.setVisibility(View.INVISIBLE);
            } else {
                btnNext.setVisibility(View.VISIBLE);

            }
        } else if (initialPage == operation.NEXT_PAGE) {
            currentPage++;

            if (currentPage >= totalPage) {
                btnNext.setVisibility(View.INVISIBLE);
                btnPrev.setVisibility(View.VISIBLE);
                if (currentPage > totalPage)
                    currentPage = totalPage;    // No page
            } else {
                btnNext.setVisibility(View.VISIBLE);
                btnPrev.setVisibility(View.VISIBLE);
            }
        } else

        {
            currentPage--;
            if (currentPage <= 1) {
                btnNext.setVisibility(View.VISIBLE);
                btnPrev.setVisibility(View.INVISIBLE);
                if (currentPage < 1)
                    currentPage = 1;    // No page
            } else {
                btnNext.setVisibility(View.VISIBLE);
                btnPrev.setVisibility(View.VISIBLE);
            }
        }

        updateServiceOnUI(); // Update UI

        Log.i(TAG, "afterUpdate: currentPage = " + currentPage + ", totalPage = " + totalPage);

    }

    private void updateServiceOnUI() {
        if (totalPage == 0) {
            tvStopName.setText("Information N/A");
            tvDesc.setText(getString(R.string.dest));
            tvMinToDepart.setText(getString(R.string.min_depart));
            tvNextService.setText(getString(R.string.next_service));
            tvType.setText(getString(R.string.type));
//            tvDistance.setText(getString(R.string.distance));
        } else {

            Date timeArrival = convertUTCTime(nextService.getValues()[currentPage - 1].getTime_timetable_utc());
            String arrivalTime = formatDate(timeArrival, "HH:mm, EEE");

            tvStopName.setText(nearbyStop.getResult().getLocation_name());
            tvNextService.setText(getString(R.string.next_service) + " " + arrivalTime);
            tvDesc.setText(getString(R.string.dest) + " " + nextService.getValues()[currentPage - 1].getRun().getDestination_name());
            tvMinToDepart.setText(getString(R.string.min_depart) + " " + getMinuteToDepart(timeArrival));
            tvType.setText(getString(R.string.type) + " " + nextService.getValues()[currentPage - 1].getRun().getTransport_type());
//            tvDistance.setText(getString(R.string.distance) + NearbyStop.convertDistance(nearbyStop.getResult().getDistance()) + "m");


        }

    }

    private String getMinuteToDepart(Date date) {

        long diff = date.getTime() - new Date().getTime();
        long min = diff / 1000 / 60;

        if (min > 60) {
            return new DecimalFormat("#.00").format(min / 60.0) + " hr";
        } else {
            return (int) min + " min";
        }

    }

    private Date convertUTCTime(String time_timetable_utc) {
        DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Date date = null;
        try {
            date = utcFormat.parse(time_timetable_utc);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

    private String formatDate(Date date, String format) {
        if (date == null)
            return "N/A";
        DateFormat utcFormat = new SimpleDateFormat(format);
        return utcFormat.format(date);
    }


    private void addMarker(double longitude, double latitude, String content, color color) {

        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions option = new MarkerOptions();
        option.position(latLng);

        if (content.length() >= 0) {
            option.title(content);
        }

        if (color == StopDetailActivity.color.GREEN) {
            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (color == StopDetailActivity.color.RED) {
            option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f));
        mMap.addMarker(option);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            case R.id.action_stored_stops:
                Intent intent = new Intent(this, RecordActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
