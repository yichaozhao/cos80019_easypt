package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Run extends Entity{
    private String num_skipped;

    private String transport_type;

    private String run_id;

    private String destination_name;

    private String destination_id;

    public String getNum_skipped ()
    {
        return num_skipped;
    }

    public void setNum_skipped (String num_skipped)
    {
        this.num_skipped = num_skipped;
    }

    public String getTransport_type ()
    {
        return transport_type;
    }

    public void setTransport_type (String transport_type)
    {
        this.transport_type = transport_type;
    }

    @Override
    public String toString() {
        return "Run{" +
                "num_skipped='" + num_skipped + '\'' +
                ", transport_type='" + transport_type + '\'' +
                ", run_id='" + run_id + '\'' +
                ", destination_name='" + destination_name + '\'' +
                ", destination_id='" + destination_id + '\'' +
                '}';
    }

    public String getRun_id ()
    {
        return run_id;
    }

    public void setRun_id (String run_id)
    {
        this.run_id = run_id;
    }

    public String getDestination_name ()
    {
        return destination_name;
    }

    public void setDestination_name (String destination_name)
    {
        this.destination_name = destination_name;
    }

    public String getDestination_id ()
    {
        return destination_id;
    }

    public void setDestination_id (String destination_id)
    {
        this.destination_id = destination_id;
    }
}
