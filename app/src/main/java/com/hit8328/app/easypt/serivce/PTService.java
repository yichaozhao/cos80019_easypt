package com.hit8328.app.easypt.serivce;

import android.location.Location;
import android.util.Log;

import com.google.gson.Gson;
import com.hit8328.app.easypt.domain.MykiStore;
import com.hit8328.app.easypt.domain.NearbyStop;
import com.hit8328.app.easypt.domain.NextService;

import org.json.JSONException;
import org.json.JSONObject;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by yichao on 17/11/14.
 */
public class PTService {


    private static final String KEY = "81487a04-54e6-11e4-b664-0665401b7368";
    private static final int DEVELOPMENT_ID = 1000299;
    private static final String TAG = "PTService";
    private static final String BASE_URL = "http://timetableapi.ptv.vic.gov.au";

    /**
     * Get the nearest services
     */
    public NearbyStop[] getServices(Location location) {
        try {
            String uri = "/v2/nearme/latitude/" + location.getLatitude() + "/longitude/" + location.getLongitude();
            Log.i(TAG, "getServices: request u/ri : " + uri);
            StringBuffer sb = requestByUri(uri);
            NearbyStop[] nearbyStops = getStops(sb);
            return nearbyStops;
        } catch (Exception ex) {
            return null;
        }
    }

    public NearbyStop[] getServices(String match) {

        match = covertString(match);

        String uri = "/v2/search/" + match;
        Log.i(TAG, "getServices: request uri : " + uri);

        StringBuffer sb = requestByUri(uri);

        NearbyStop[] nearbyStops = getStops(sb);
        return nearbyStops;
    }

    private String covertString(String match) {

        return match.replace(" ", "%20");
    }

    public NearbyStop[] getServices(Location location, float threshold) {
        return null;

    }

    private NearbyStop[] getStops(StringBuffer sb) {
        if (sb == null)
            return null;

        Gson gson = new Gson();

        NearbyStop[] nearbyStops = gson.fromJson(sb.toString(), NearbyStop[].class);
        Log.i(TAG, "parseResult = " + nearbyStops.length);
        return nearbyStops;
    }

    public NearbyStop[] getServices(Location location, float threshold, String match) {
        return null;

    }

    public NextService getNextService(String type, String stopId) {
        String uri = "/v2/mode/" + getType(type) + "/stop/" + stopId + "/departures/by-destination/limit/1";
        StringBuffer sb = requestByUri(uri);

        if (sb == null)
            return null;


        Gson gson = new Gson();

        NextService nextService = gson.fromJson(sb.toString(), NextService.class);
        Log.i(TAG, "parseResult = " + nextService.toString());
        return nextService;

    }

    public MykiStore getMykiStore(Location currLoc, double range) {

        String uri = "/v2/poi/100/lat1/" + (currLoc.getLatitude() - range) + "/long1/" + (currLoc.getLongitude() - range) + "/lat2/" + (currLoc.getLatitude() + range) + "/long2/" + (currLoc.getLongitude() + range) + "/griddepth/1/limit/10";
        MykiStore stores = null;
        StringBuffer sb = requestByUri(uri);
        try {

            if (sb == null)
                return null;

            Gson gson = new Gson();
            stores = gson.fromJson(sb.toString(), MykiStore.class);
        } catch (Exception ex) {
            return null;
        }


        return stores;
    }

    private int getType(String type) {

        Log.i(TAG, "getType: type = " + type);
        if (type.toLowerCase().equals("bus")) {
            return 2;
        } else if (type.toLowerCase().equals("nightrider")) {
            return 4;
        } else if (type.toLowerCase().equals("train")) {
            return 0;
        } else if (type.toLowerCase().equals("tram")) {
            return 1;
        } else {
            return 3;
        }
    }


    /**
     * Request By Uri
     *
     * @param uri uri for request
     * @return JSON string requested by uri
     */
    private StringBuffer requestByUri(String uri) {

        if (healthCheck()) {

            URL url;
            InputStream in = null;

            StringBuffer sb = new StringBuffer();
            try {
//                    signature+= "&timestamp=" + nowAsISO;
                url = new URL(generateCompleteURLWithSignature(KEY, uri, DEVELOPMENT_ID));
                Log.i(TAG, "getServices url = " + generateCompleteURLWithSignature(KEY, uri, DEVELOPMENT_ID));

                URLConnection urlConnection = url.openConnection();

                BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
                try {
                    String content = null;
                    while ((content = br.readLine()) != null) {
                        sb.append(content);
                    }

                    // Parse into JSON Format
                    Log.i(TAG, "content read = " + sb.toString());

                    return sb;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            return null;
        }
        return null;
    }


    private boolean healthCheck() {

        TimeZone tz = TimeZone.getTimeZone("UTC");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        df.setTimeZone(tz);
        String nowAsISO = df.format(new Date());


        String uri = "/v2/HealthCheck?timestamp=" + nowAsISO;
        URL url = null;
        InputStream in = null;

        StringBuffer sb = new StringBuffer();
        try {
            String signature = generateCompleteURLWithSignature(KEY, uri, DEVELOPMENT_ID);
//                    signature+= "&timestamp=" + nowAsISO;
            url = new URL(signature);
            URLConnection urlConnection = url.openConnection();

            Log.i(TAG, "url = " + signature);
            BufferedReader br = new BufferedReader(new InputStreamReader(new BufferedInputStream(urlConnection.getInputStream())));
            try {
                String content = null;
                while ((content = br.readLine()) != null) {
                    sb.append(content);
                }

                // Parse into JSON Format
//                Log.i(TAG, "content read = " + sb.toString());
                JSONObject jsobj = new JSONObject(sb.toString());
//                Log.i(TAG, "JSON Object = " + jsobj);
                return jsobj.getBoolean("databaseOK") && jsobj.getBoolean("memcacheOK") && jsobj.getBoolean("clientClockOK") && jsobj.getBoolean("securityTokenOK");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;

    }

    /**
     * Generate full URL using generateSignature() method
     *
     * @param privateKey - Developer Key supplied by PTV (Example : "92dknhh31-6a30-4cac- 8d8b-8a1970834799");
     * @param uri        - request uri (Example :"/v2/mode/2/line/787/stops-for-line) * @param developerId - Developer  supplied by PTV( int developerId ) * @return - Full URL with Signature
     */
    private String generateCompleteURLWithSignature(final String privateKey, final String uri, final int developerId) {
        String baseURL = "http://timetableapi.ptv.vic.gov.au";
        StringBuffer url = new StringBuffer(baseURL).append(uri).append(uri.contains("?") ? "&" : "?").append("devid=" + developerId).append("&signature=" + generateSignature(privateKey, uri, developerId));

        return url.toString();
    }

    /**
     * Generates a signature using the HMAC-SHA1 algorithm *
     *
     * @param privateKey - Developer Key supplied by PTV * @param uri - request uri (Example :/v2/HealthCheck) * @param developerId - Developer ID supplied by PTV * @return Unique Signature Value
     */
    private String generateSignature(final String privateKey, final String uri, final int developerId) {
        String encoding = "UTF-8";
        String HMAC_SHA1_ALGORITHM = "HmacSHA1";
        String signature;
        StringBuffer uriWithDeveloperID = new StringBuffer();
        uriWithDeveloperID.append(uri).append(uri.contains("?") ? "&" : "?").append("devid=" + developerId);
        try {
            byte[] keyBytes = privateKey.getBytes(encoding);
            byte[] uriBytes = uriWithDeveloperID.toString().getBytes(encoding);
            Key signingKey = new SecretKeySpec(keyBytes, HMAC_SHA1_ALGORITHM);
            Mac mac = Mac.getInstance(HMAC_SHA1_ALGORITHM);
            mac.init(signingKey);
            byte[] signatureBytes = mac.doFinal(uriBytes);
            StringBuffer buf = new StringBuffer(signatureBytes.length * 2);
            for (byte signatureByte : signatureBytes) {
                int intVal = signatureByte & 0xff;
                if (intVal < 0x10) {
                    buf.append("0");
                }
                buf.append(Integer.toHexString(intVal));
            }
            signature = buf.toString();
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        } catch (InvalidKeyException e) {
            throw new RuntimeException(e);
        }
        return signature.toString().toUpperCase();
    }


}

