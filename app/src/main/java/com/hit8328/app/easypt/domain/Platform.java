package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Platform extends Entity{
    private Stop stop;

    private Direction direction;

    private String realtime_id;

    public Stop getStop ()
    {
        return stop;
    }

    public void setStop (Stop stop)
    {
        this.stop = stop;
    }

    public Direction getDirection ()
    {
        return direction;
    }

    public void setDirection (Direction direction)
    {
        this.direction = direction;
    }

    public String getRealtime_id ()
    {
        return realtime_id;
    }

    public void setRealtime_id (String realtime_id)
    {
        this.realtime_id = realtime_id;
    }

    @Override
    public String toString() {
        return "Platform{" +
                "stop=" + stop +
                ", direction=" + direction +
                ", realtime_id='" + realtime_id + '\'' +
                '}';
    }
}
