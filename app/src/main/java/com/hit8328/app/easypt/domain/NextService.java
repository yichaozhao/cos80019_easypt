package com.hit8328.app.easypt.domain;

import java.util.Arrays;

/**
 * Created by yichao on 17/11/14.
 */
public class NextService extends Entity{
    private Values[] values;

    public Values[] getValues ()
    {
        return values;
    }

    @Override
    public String toString() {
        return "NextService{" +
                "values=" + Arrays.toString(values) +
                '}';
    }

    public void setValues (Values[] values)
    {
        this.values = values;
    }
}
