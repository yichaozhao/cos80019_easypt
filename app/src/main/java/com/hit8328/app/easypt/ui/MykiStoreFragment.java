package com.hit8328.app.easypt.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.Locations;
import com.hit8328.app.easypt.domain.MykiStore;
import com.hit8328.app.easypt.serivce.PTService;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class MykiStoreFragment extends Fragment {

    private static final String TAG = "MykiStoreFragment";
    private GoogleMap mMap;
    private MykiStore stores;
    private Location currLoc;

    private LocationManager locationManager;
    private ProgressDialog progress;
    private static View view;

    public MykiStoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_myki_nearby, container, false);
        } catch (InflateException e) {
        /* map is already there, just return view as it is */
        }

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Getting Information");
        progress.setMessage("Please Wait....");
        progress.setIndeterminate(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

        // Register Location Service
        // Register listener with Location Manager to receive location updates
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
                0, 0, locationListener);

        // Get Current Location
        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
         mMap = ((MapFragment) getActivity().getFragmentManager().findFragmentById(R.id.myki_map)).getMap();
        new ObtainMykiStoreTask().execute();

        return view;

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity)activity).onSectionAttached(2);

    }

    private class ObtainMykiStoreTask  extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            progress.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            stores = new PTService().getMykiStore(currLoc, 0.02);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // Display marks on the Map
            progress.dismiss();

            showOnMap(stores);
        }
    }


    /**
     * Define a listener that responds to location updates
     */
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            updateLocationOnUI(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status == LocationProvider.AVAILABLE) {
                updateLocationOnUI();
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };


    /**
     * Get last known location and then display it
     */
    private void updateLocationOnUI() {
        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
    }

    /**
     * Display date and location data
     */
    private void updateLocationOnUI(Location location) {
        if (location != null) {

            String locNow = (new Date()) + ", " + location.toString();
            Toast.makeText(getActivity(), locNow, Toast.LENGTH_SHORT).show();
        }
    }

    private void showOnMap(MykiStore stores) {
        if(stores != null){


            // Stores Location
            for(Locations location: stores.getLocations()){
                String content = location.getBusiness_name();
                LatLng latLng = new LatLng(Double.parseDouble(location.getLat()), Double.parseDouble(location.getLon()));
                Log.i(TAG, "showOnMap: " + latLng);
                MarkerOptions option = new MarkerOptions();
                option.position(latLng);
                option.title(content);
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12.5f));
                mMap.addMarker(option);
            }



        }else{
            Toast.makeText(getActivity(), "Please check Internet", Toast.LENGTH_SHORT).show();
        }

        // Put Current Location
        LatLng latLng = new LatLng(currLoc.getLatitude(),currLoc.getLongitude());
        MarkerOptions option = new MarkerOptions();
        option.position(latLng);

        option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13f));
        mMap.addMarker(option);
    }


}
