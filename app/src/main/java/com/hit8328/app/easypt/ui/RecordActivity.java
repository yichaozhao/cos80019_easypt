package com.hit8328.app.easypt.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.google.gson.Gson;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.Record;
import com.hit8328.app.easypt.serivce.CriteriaExpired;
import com.hit8328.app.easypt.serivce.Standard;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.Inflater;

/**
 * Created by yichao on 18/11/14.
 */
public class RecordActivity extends Activity implements AdapterView.OnItemClickListener {

    private static final String TAG = "RecordActivity";
    ListView listView;
    Record[] records;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.record_menu, menu);
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_record);
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.record_list);

        Set<Record> record = obtainRecords();


        if (record != null) {
            records = new Record[record.size()];
            record.toArray(records);
            String[] result = convertToString(record);
            listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, result));
            listView.setOnItemClickListener(this);
        } else {
            listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new String[]{}));
        }
    }

    private String[] convertToString(Set<Record> records) {
        String[] ret = new String[records.toArray().length];
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        int counter = 0;
        for (Record record : records) {

            ret[counter++] = record.getStopName() + "\n" + record.getDestination() + "\n" + simpleDateFormat.format(record.getTimeArrival());
        }

        return ret;
    }


    private Set<Record> obtainRecords() {

        Gson gson = new Gson();

        // Get Preference
        SharedPreferences settings = getSharedPreferences(Standard.PREF_FILE, 0);
        String jsonStr = settings.getString("record", "");   // Json Sting

        if (jsonStr.length() == 0)
            return null;

        // Get the Set
        Record[] records = gson.fromJson(jsonStr, Record[].class);
        Set set = new CriteriaExpired().meetCriteria(records);

        Log.i(TAG, "Number: " + set.toArray().length + ", saveRecord records = " + set + "JSON = " + jsonStr);

        // Save in the Preference
        SharedPreferences.Editor editor = settings.edit();
        // Save to Preference
        jsonStr = gson.toJson(set.toArray());
        editor.putString("record", jsonStr);
        editor.commit();

        return set;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            case R.id.action_clear_history:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                AlertDialog dialog = builder.setIcon(R.drawable.ic_launcher)
                        .setTitle(R.string.delete_title)
                        .setMessage("Are you sure?")
                        .setNegativeButton(R.string.btnCancel_content, new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setPositiveButton(R.string.btnClear_content, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                // Get Preference
                                SharedPreferences settings = getSharedPreferences(Standard.PREF_FILE, 0);
                                SharedPreferences.Editor editor = settings.edit();
                                editor.putString("record", "");
                                editor.apply();

                                // Show to the User
                                Toast.makeText(RecordActivity.this, "The records have been deleted.", Toast.LENGTH_SHORT).show();
                                // Refresh
                                listView.setAdapter(new ArrayAdapter<String>(RecordActivity.this, android.R.layout.simple_list_item_1, new String[]{}));
                            }
                        }).create();
                dialog.show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, StopMapActivity.class);
        intent.putExtra("map_stop", records[position]);
        this.startActivity(intent);
    }
}
