package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Values extends Entity{
    private Platform platform;

    private String flags;

    private String time_realtime_utc;

    private Run run;

    private String time_timetable_utc;

    public Platform getPlatform ()
    {
        return platform;
    }

    public void setPlatform (Platform platform)
    {
        this.platform = platform;
    }

    public String getFlags ()
    {
        return flags;
    }

    public void setFlags (String flags)
    {
        this.flags = flags;
    }

    public String getTime_realtime_utc ()
    {
        return time_realtime_utc;
    }

    public void setTime_realtime_utc (String time_realtime_utc)
    {
        this.time_realtime_utc = time_realtime_utc;
    }

    public Run getRun ()
    {
        return run;
    }

    public void setRun (Run run)
    {
        this.run = run;
    }

    public String getTime_timetable_utc ()
    {
        return time_timetable_utc;
    }

    public void setTime_timetable_utc (String time_timetable_utc)
    {
        this.time_timetable_utc = time_timetable_utc;
    }

    @Override
    public String toString() {
        return "Values{" +
                "platform=" + platform +
                ", flags='" + flags + '\'' +
                ", time_realtime_utc='" + time_realtime_utc + '\'' +
                ", run=" + run +
                ", time_timetable_utc='" + time_timetable_utc + '\'' +
                '}';
    }
}
