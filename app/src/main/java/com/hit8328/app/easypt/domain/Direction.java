package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Direction extends Entity{
    private String direction_id;

    private String linedir_id;

    private Line line;

    private String direction_name;

    public String getDirection_id ()
    {
        return direction_id;
    }

    public void setDirection_id (String direction_id)
    {
        this.direction_id = direction_id;
    }

    public String getLinedir_id ()
    {
        return linedir_id;
    }

    public void setLinedir_id (String linedir_id)
    {
        this.linedir_id = linedir_id;
    }

    public Line getLine ()
    {
        return line;
    }

    public void setLine (Line line)
    {
        this.line = line;
    }

    public String getDirection_name ()
    {
        return direction_name;
    }

    public void setDirection_name (String direction_name)
    {
        this.direction_name = direction_name;
    }

    @Override
    public String toString() {
        return "Direction{" +
                "direction_id='" + direction_id + '\'' +
                ", linedir_id='" + linedir_id + '\'' +
                ", line=" + line +
                ", direction_name='" + direction_name + '\'' +
                '}';
    }
}
