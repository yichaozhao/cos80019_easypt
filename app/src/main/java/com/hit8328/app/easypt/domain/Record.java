package com.hit8328.app.easypt.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by yichao on 18/11/14.
 */
public class Record extends Entity implements Serializable{


    private static final long serialVersionUID = -6298362482292197621L;

    private String stopName;
    private Date timeArrival;
    private String destination;
    private String type;
    private double longtitute;
    private double latitute;

    public double getLongtitute() {
        return longtitute;
    }

    public void setLongtitute(double longtitute) {
        this.longtitute = longtitute;
    }

    public double getLatitute() {
        return latitute;
    }

    public void setLatitute(double latitute) {
        this.latitute = latitute;
    }

    public Record(String stopName, Date timeArrival, String destination, String type, double longtitute, double latitute) {
        this.stopName = stopName;
        this.timeArrival = timeArrival;
        this.destination = destination;
        this.type = type;
        this.latitute = latitute;
        this.longtitute =longtitute;
    }

    public Record() {
    }

    public String getStopName() {
        return stopName;
    }

    public void setStopName(String stopName) {
        this.stopName = stopName;
    }

    public Date getTimeArrival() {
        return timeArrival;
    }

    public void setTimeArrival(Date timeArrival) {
        this.timeArrival = timeArrival;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Record{" +
                "stopName='" + stopName + '\'' +
                ", timeArrival=" + timeArrival +
                ", destination='" + destination + '\'' +
                ", type='" + type + '\'' +
                ", longtitute=" + longtitute +
                ", latitute=" + latitute +
                '}';
    }

    @Override
    public int hashCode() {
        int result = stopName != null ? stopName.hashCode() : 0;
        result = 31 * result + (timeArrival != null ? timeArrival.hashCode() : 0);
        result = 31 * result + (destination != null ? destination.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
