package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 18/11/14.
 */
public class Locations extends Entity{
    private String business_name;

    private String distance;

    private String lon;

    private String location_name;

    private String outlet_type;

    private String suburb;

    private String lat;

    public String getBusiness_name ()
    {
        return business_name;
    }

    public void setBusiness_name (String business_name)
    {
        this.business_name = business_name;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getLocation_name ()
    {
        return location_name;
    }

    public void setLocation_name (String location_name)
    {
        this.location_name = location_name;
    }

    public String getOutlet_type ()
    {
        return outlet_type;
    }

    public void setOutlet_type (String outlet_type)
    {
        this.outlet_type = outlet_type;
    }

    public String getSuburb ()
    {
        return suburb;
    }

    public void setSuburb (String suburb)
    {
        this.suburb = suburb;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }
}
