package com.hit8328.app.easypt.serivce;

import com.hit8328.app.easypt.domain.Entity;
import com.hit8328.app.easypt.domain.Record;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by yichao on 18/11/14.
 */
public interface Criteria {
    public Set<Entity> meetCriteria(Entity[] entities);

}
