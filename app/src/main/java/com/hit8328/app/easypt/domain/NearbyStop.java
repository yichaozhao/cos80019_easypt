package com.hit8328.app.easypt.domain;

import android.util.Log;

import java.io.Serializable;
import java.text.DecimalFormat;

/**
 * Created by yichao on 17/11/14.
 */
public class NearbyStop extends Entity implements Serializable  {

    private static final long serialVersionUID = 7594258502700426346L;
    private static final String TAG = "NearbyStop";

    private Result result;

    private String type;

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public NearbyStop() {
    }

    @Override
    public String toString() {
        return "Stop{" +
                "result=" + result +
                ", type='" + type + '\'' +
                '}';
    }


    public static String[] getStopNamesFromObj(NearbyStop[] nearbyStops) {

        if (nearbyStops == null)
            return null;
        String[] names = new String[nearbyStops.length];
        for (int i = 0; i < nearbyStops.length; i++) {
            Log.i(TAG, "getStopNamesFromObj: i = " + i);
            String distance = convertDistance(nearbyStops[i].getResult().getDistance());
            String.format(String.format(names[i] = nearbyStops[i].getResult().getTransport_type() + " - " + (nearbyStops[i].getResult().getLocation_name() == null ? "Unknown Location" : nearbyStops[i].getResult().getLocation_name() + "(" + distance + " m)")));
        }

        return names;

    }

    public static String convertDistance(double distance){
        return new DecimalFormat("#.00").format(distance * 1.609 * 10000000);

    }
}
