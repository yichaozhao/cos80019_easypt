package com.hit8328.app.easypt.ui;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.NearbyStop;
import com.hit8328.app.easypt.serivce.PTService;
import com.hit8328.app.easypt.serivce.Standard;

/*
 * NOTES:
 * 1. Safer to use Fragment rather than ListFragment
 */
public class MainActivity extends FragmentActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String TAG = "MainActivity";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;
    private int sectionNumber;
    private ProgressDialog progress;
    private boolean doubleBackToExitPressedOnce;

//    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

//        btn = (Button) this.findViewById(R.id.btn_test);
//        btn.setOnClickListener(this);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {

        Log.i(TAG, "onNavigationDrawerItemSelected , position = " + position);
        // update the main content by replacing fragments
        FragmentManager manager = getSupportFragmentManager();

        Fragment fragment = null;
        // Get the replaced fragment
        switch (position) {
            case 0:
                fragment = new TrainTimetableFragment();
                break;
            case 1:
                fragment = new StopsNearbyFragment();
                break;
            case 2:
                fragment = new MykiStoreFragment();
                break;

        }

        if (null != fragment) {
            manager.beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sectionNumber = savedInstanceState.getInt("section");
        onNavigationDrawerItemSelected(sectionNumber);

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("section", sectionNumber);
        super.onSaveInstanceState(outState);
    }

    public void onSectionAttached(int number) {

        Log.i(TAG, "onSectionAttached, number = " + number);
        sectionNumber = number;
        switch (number) {
            case 0:
                mTitle = getString(R.string.title_section1);
                break;
            case 1:
                mTitle = getString(R.string.title_section2);
                break;
            case 2:
                mTitle = getString(R.string.title_section3);
                break;
        }
    }

    public void restoreActionBar() {

        Log.i(TAG, "restoreActionBar: mTitle = " + mTitle);
        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "onCreateOptionsMenu");
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            if (sectionNumber == 1)
                getMenuInflater().inflate(R.menu.menu_stop_nearby, menu);
            else
                getMenuInflater().inflate(R.menu.main, menu);

            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();
        if (id == R.id.action_stored_stops) {
            // Open New Activity
            Intent intent = new Intent(this, RecordActivity.class);
            this.startActivity(intent);

            return true;
        } else if (id == R.id.action_search) {
            return searchByStopName();
        } else if (id == R.id.action_setting) {
            // Read the Pref
            final SharedPreferences settings = getSharedPreferences(Standard.PREF_FILE, 0);
            final int selects = settings.getInt("setting", 0);

            Log.i(TAG, "1--------Select = " + selects);

            boolean[] defVal = toBooleanArray(selects);
            // Open Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog dialog = builder.setIcon(R.drawable.ic_launcher)
                    .setTitle(R.string.setting_title)
                    .setNegativeButton(R.string.btnCancel_content, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Clear the Cache
                            int oldVal = settings.getInt("setting", 0);

                            SharedPreferences.Editor editor = settings.edit();
                            editor.putInt("settingCache", oldVal);
                            editor.apply();
                        }
                    })
                    .setPositiveButton(R.string.btnSave_content, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Update to the Pref
                            int cacheSelects = settings.getInt("settingCache", 0);

                            SharedPreferences.Editor editor = settings.edit();
                            editor.putInt("setting", cacheSelects);
                            editor.apply();
                        }
                    })
                    .setMultiChoiceItems(new CharSequence[]{"Train", "Tram", "Bus", "V/Line and Coach", "NightRider"}, defVal, new DialogInterface.OnMultiChoiceClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            int cacheSelects = settings.getInt("settingCache", 0);
                            Log.i(TAG, "which = " + which);
                            switch (which) {
                                case 0:
                                    cacheSelects = isChecked ? (cacheSelects | 0b1) : (cacheSelects & 0b11110);
                                    break;
                                case 1:
                                    cacheSelects = isChecked ? (cacheSelects | 0b10) : (cacheSelects & 0b11101);
                                    break;
                                case 2:
                                    cacheSelects = isChecked ? (cacheSelects | 0b100) : (cacheSelects & 0b11011);
                                    break;
                                case 3:
                                    cacheSelects = isChecked ? (cacheSelects | 0b1000) : (cacheSelects & 0b10111);
                                    break;
                                case 4:
                                    cacheSelects = isChecked ? (cacheSelects | 0b10000) : (cacheSelects & 0b01111);
                                    break;
                            }
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putInt("settingCache", cacheSelects);
                            Log.i(TAG, "2---------settingCache = " + cacheSelects);

                            editor.apply();
                        }
                    }).create();       // Train - Tram - Bus - V/Line - NightRider
            dialog.show();
        } else if (id == R.id.action_clear_history) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            AlertDialog dialog = builder.setIcon(R.drawable.ic_launcher)
                    .setTitle(R.string.delete_title)
                    .setMessage("Are you sure?")
                    .setNegativeButton(R.string.btnCancel_content, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setPositiveButton(R.string.btnClear_content, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // Get Preference
                            SharedPreferences settings = getSharedPreferences(Standard.PREF_FILE, 0);
                            SharedPreferences.Editor editor = settings.edit();
                            editor.putString("record", "");
                            editor.apply();

                            // Show to the User
                            Toast.makeText(MainActivity.this, "The records have been deleted.", Toast.LENGTH_SHORT).show();
                        }
                    }).create();
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }



    private boolean[] toBooleanArray(int selects) {
        return new boolean[]{(selects & 0b1) == 0b01, (selects & 0b10) == 0b010, (selects & 0b100) == 0b0100, (selects & 0b1000) == 0b01000, (selects & 0b10000) == 0b010000};
    }

    private boolean searchByStopName() {
        final EditText input = new EditText(this);                // Prepare an EditText
        input.setHint("Type here...");
        // Create Dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher)
                .setTitle(R.string.dialog_title)
                .setMessage("Please type in the keywords")
                .setNegativeButton(R.string.btnCancel_content, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).setPositiveButton(R.string.btnOk_content, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.i(TAG, "Search Button");
                String searchText = input.getText().toString().trim();

                new ObtainServiceTask().execute(searchText);

            }
        });

        AlertDialog alert = builder.create();
        alert.setView(input);
        alert.show();
        return true;
    }

    private class ObtainServiceTask extends AsyncTask<String, Void, NearbyStop[]> {

        @Override
        protected void onPreExecute() {
            if (progress == null) {
                progress = new ProgressDialog(MainActivity.this);
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setTitle("Searching Stops");
                progress.setMessage("Please Wait..");
            }

            progress.show();
        }


        @Override
        protected NearbyStop[] doInBackground(String... params) {
//            Log.i(TAG, new PTService().healthCheck() ? "a" : "b");
            NearbyStop[] nearbyStops = new PTService().getServices(params[0]);
            return nearbyStops;
        }

        @Override
        protected void onPostExecute(NearbyStop[] nearbyStops) {

            // Display on the List
            // Close the Progress Dialog
            progress.dismiss();

            // Update the view
            if (nearbyStops!= null && getSupportFragmentManager().findFragmentById(R.id.container) instanceof StopsNearbyFragment) {
                // Safe Check
                ListView list = (ListView) MainActivity.this.findViewById(R.id.stop_nearby);
                list.setAdapter(new ArrayAdapter<String>(MainActivity.this.getApplicationContext(), android.R.layout.simple_list_item_1, NearbyStop.getStopNamesFromObj(nearbyStops)));
                list.setBackgroundColor(0xCCCC8B4D);

                // Save the state in the Fragment
                StopsNearbyFragment fragment = (StopsNearbyFragment) getSupportFragmentManager().findFragmentById(R.id.container);
                fragment.setNearbyStops(nearbyStops);

            }else{
                Toast.makeText(MainActivity.this, "Please check Internet...", Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        if(sectionNumber != 0){
            onNavigationDrawerItemSelected(0);  // Select the first fragment
        }else{
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }
}


