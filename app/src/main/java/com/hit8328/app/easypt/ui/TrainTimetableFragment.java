package com.hit8328.app.easypt.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.hit8328.app.easypt.R;
import net.sf.andpdf.pdfviewer.PdfViewerActivity;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;


/**
 * A simple {@link Fragment} subclass.
 */
public class TrainTimetableFragment extends Fragment {

    private static final String TAG = "TrainTimetableFragment";

    private ProgressDialog progress;

    private String[] trainLines;

    public TrainTimetableFragment() {
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.i(TAG, "PlaceholderFragment - > onAttach");
        ((MainActivity) activity).onSectionAttached(0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.i(TAG, "onCreateView");
        View view = inflater.inflate(R.layout.fragment_train_timetable, container, false);

        // Create the Progress Bar
        trainLines = getResources().getStringArray(R.array.trains);
        ListView list = (ListView) view.findViewById(R.id.train_list);
        list.setOnItemClickListener(new ItemClickListner());

        return view;
    }


    private boolean isExist(String fileName) {

//        File myFile = new File(extStore.getAbsolutePath() + "/Download/" + fileName);
        try {
            InputStream input = getActivity().openFileInput(fileName);
            return input != null;
        } catch (FileNotFoundException e) {
            Log.i(TAG, "Message = " + e.getMessage());
            return false;
        }
//        return myFile.exists();
    }

    class FileDownloadTask extends AsyncTask<String, String, String> {
        boolean isError = false;
        String fileName;

        @Override
        protected void onPreExecute() {
            progress.show();
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream input = null;
            FileOutputStream output = null;
            int count;
            try {
                URL url = new URL(params[0]);
                fileName = params[1];


                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                Log.i(TAG, "doInBackground, url = " + url + ", filePath = " + fileName + ", length = " + lenghtOfFile);
                // download the file
                input = new BufferedInputStream(url.openStream(), 8192);
                // Output stream
                output = getActivity().openFileOutput(fileName, Context.MODE_PRIVATE);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    Log.i(TAG, "publicProgress = " + "" + (int) ((total * 100) / lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }


            } catch (Exception e) {
                Log.e(TAG, "Message = " + e.getMessage());
                isError = true;

            } finally {
                try {

                    // flushing output
                    if (output != null) {
                        output.flush();
                        // closing streams
                        output.close();
                    }
                    if (input != null)
                        input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return fileName;
        }

        /**
         * Updating progress bar
         */

        protected void onProgressUpdate(String... percentage) {
            Log.i(TAG, "onProgressUpdate: percentage = " + percentage[0]);
            // setting progress percentage
            progress.setProgress(Integer.parseInt(percentage[0]));
        }

        @Override
        protected void onPostExecute(String filePath) {
            Log.i(TAG, "onPostExecute");
            // dismiss the dialog after the file was downloaded

            if (progress != null)
                progress.dismiss();

            if (isError) {
                Toast.makeText(getActivity(), "Please Check Internet... ", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(getActivity(), "The pdf has been saved in " + filePath, Toast.LENGTH_SHORT).show();
                openPDFFile(filePath);
            }


        }
    }

    private class ItemClickListner implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Log.i(TAG, "onItemClick->position: " + position);

//            Toast.makeText(getActivity(), "Clicked Position = " + position, Toast.LENGTH_SHORT).show();
            String train = getResources().getStringArray(R.array.train_pdf)[position];
            String fileName = "02" + train + "_ttb_TP.pdf";   // File Name

            if (!isExist(fileName)) {
                // Download the pdf file
                String url = "http://tt.ptv.vic.gov.au/tt/TTB/20141112-144339/vic/" + fileName;
                //Create the Dialog
                progress = new ProgressDialog(getActivity());
                progress.setTitle("Downloading the PDF Timetable");
                progress.setMessage("Please Wait....");
                progress.setIndeterminate(false);
                progress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progress.setMax(100);


                new FileDownloadTask().execute(url, fileName);

                Log.i(TAG, "url : " + url);

            }else{
                openPDFFile(fileName);
            }
        }
    }

    private void openPDFFile(String fileName) {

        Intent intent = new Intent(getActivity(), MyPdfViewerActivity.class);

        intent.putExtra(PdfViewerActivity.EXTRA_PDFFILENAME,
                getActivity().getFileStreamPath(fileName).getPath());
        startActivity(intent);
    }
}
