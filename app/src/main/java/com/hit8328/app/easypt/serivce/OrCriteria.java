package com.hit8328.app.easypt.serivce;

import com.hit8328.app.easypt.domain.Entity;
import com.hit8328.app.easypt.domain.NearbyStop;

import java.util.*;

/**
 * Created by yichao on 18/11/14.
 */
public class OrCriteria implements Criteria {

    List<Criteria> criterias;

    public OrCriteria(List<Criteria> criterias) {
        this.criterias = criterias;
    }

    @Override
    public Set<Entity> meetCriteria(Entity[] entities) {

        Set<Entity> retSet = new HashSet<Entity>();

        for (Criteria criteria : this.criterias) {
            Set<Entity> filteredObj = criteria.meetCriteria(entities);
            for (Entity entity : filteredObj) {
                retSet.add(entity);
            }
        }
        return retSet;
    }
}
