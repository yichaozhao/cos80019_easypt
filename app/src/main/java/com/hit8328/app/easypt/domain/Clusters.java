package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 25/11/14.
 */
public class Clusters {
    private String weightedLong;

    private String maxLong;

    private String maxLat;

    private String minLat;

    private Clusters clusters;

    private Locations[] locations;

    private String totalLocations;

    private String weightedLat;

    private String minLong;

    public String getWeightedLong ()
    {
        return weightedLong;
    }

    public void setWeightedLong (String weightedLong)
    {
        this.weightedLong = weightedLong;
    }

    public String getMaxLong ()
    {
        return maxLong;
    }

    public void setMaxLong (String maxLong)
    {
        this.maxLong = maxLong;
    }

    public String getMaxLat ()
    {
        return maxLat;
    }

    public void setMaxLat (String maxLat)
    {
        this.maxLat = maxLat;
    }

    public String getMinLat ()
    {
        return minLat;
    }

    public void setMinLat (String minLat)
    {
        this.minLat = minLat;
    }

    public Clusters getClusters ()
    {
        return clusters;
    }

    public void setClusters (Clusters clusters)
    {
        this.clusters = clusters;
    }

    public Locations[] getLocations ()
    {
        return locations;
    }

    public void setLocations (Locations[] locations)
    {
        this.locations = locations;
    }

    public String getTotalLocations ()
    {
        return totalLocations;
    }

    public void setTotalLocations (String totalLocations)
    {
        this.totalLocations = totalLocations;
    }

    public String getWeightedLat ()
    {
        return weightedLat;
    }

    public void setWeightedLat (String weightedLat)
    {
        this.weightedLat = weightedLat;
    }

    public String getMinLong ()
    {
        return minLong;
    }

    public void setMinLong (String minLong)
    {
        this.minLong = minLong;
    }
}
