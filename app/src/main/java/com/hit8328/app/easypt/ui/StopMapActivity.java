package com.hit8328.app.easypt.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.Locations;
import com.hit8328.app.easypt.domain.Record;

import java.util.Date;

/**
 * Created by yichao on 19/11/14.
 */
public class StopMapActivity extends Activity {

    private static final String TAG = "StopMapActivity";
    private GoogleMap mMap;
    private LocationManager locationManager;
    private Location currLoc;
    private Record record;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_map_stop);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);


        record = (Record) getIntent().getSerializableExtra("map_stop");

        // Register listener with Location Manager to receive location updates
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
                0, 0, locationListener);

        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);


        // Display on Map
        if (record != null) {
            showOnMap();
        }


    }

    private void showOnMap() {
        // Show on the Map
        mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.stop_map)).getMap();

        // Current Marker
        LatLng latLng = new LatLng(currLoc.getLatitude(), currLoc.getLongitude());
        MarkerOptions option = new MarkerOptions();
        option.position(latLng);
        option.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mMap.addMarker(option);

        // Stop Marker
        String content = record.getStopName();
        LatLng latLng1 = new LatLng(record.getLatitute(), record.getLongtitute());

        MarkerOptions option2 = new MarkerOptions();
        option2.position(latLng1);
        option2.title(content);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng1, 12.5f));
        mMap.addMarker(option2);

    }


    /**
     * Get last known location and then display it
     */
    private void updateLocationOnUI() {
        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        Log.i(TAG, "updateLocationOnUI currentLoc = " + currLoc);

    }

    /**
     * Display date and location data
     */
    private void updateLocationOnUI(Location location) {
        if (location != null) {

            String locNow = (new Date()) + ", " + location.toString();
            Toast.makeText(this, locNow, Toast.LENGTH_SHORT).show();
        }
    }

    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            updateLocationOnUI(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status == LocationProvider.AVAILABLE) {
                updateLocationOnUI();
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
