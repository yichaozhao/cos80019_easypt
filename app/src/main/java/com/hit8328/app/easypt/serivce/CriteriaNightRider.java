package com.hit8328.app.easypt.serivce;

import com.hit8328.app.easypt.domain.Entity;
import com.hit8328.app.easypt.domain.NearbyStop;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by yichao on 18/11/14.
 */
public class CriteriaNightRider implements Criteria{
    @Override
    public Set<Entity> meetCriteria(Entity[] entities) {
        Set<Entity> set = new HashSet<Entity>();
        for (Entity entity : entities) {
            if(((NearbyStop)entity).getResult().getTransport_type().equals("nightrider")){
                set.add(entity);
            }
        }
        return set;
    }
}
