package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Line extends Entity{
    private String line_name;

    private String transport_type;

    private String line_id;

    private String line_number;

    public String getLine_name ()
    {
        return line_name;
    }

    public void setLine_name (String line_name)
    {
        this.line_name = line_name;
    }

    public String getTransport_type ()
    {
        return transport_type;
    }

    public void setTransport_type (String transport_type)
    {
        this.transport_type = transport_type;
    }

    public String getLine_id ()
    {
        return line_id;
    }

    public void setLine_id (String line_id)
    {
        this.line_id = line_id;
    }

    public String getLine_number ()
    {
        return line_number;
    }

    public void setLine_number (String line_number)
    {
        this.line_number = line_number;
    }

    @Override
    public String toString() {
        return "Line{" +
                "line_name='" + line_name + '\'' +
                ", transport_type='" + transport_type + '\'' +
                ", line_id='" + line_id + '\'' +
                ", line_number='" + line_number + '\'' +
                '}';
    }
}
