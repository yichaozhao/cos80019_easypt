package com.hit8328.app.easypt.ui;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import com.hit8328.app.easypt.R;
import com.hit8328.app.easypt.domain.Entity;
import com.hit8328.app.easypt.domain.NearbyStop;
import com.hit8328.app.easypt.serivce.*;

import java.util.*;

public class StopsNearbyFragment extends Fragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    private static final String TAG = "StopsNearbyFragment";
    private LocationManager locationManager;
//    private Button btnTest;
    private Location currLoc;


    private NearbyStop[] nearbyStops;
    private ProgressDialog progress;
    private ListView list;

    public NearbyStop[] getNearbyStops() {
        return nearbyStops;
    }

    public void setNearbyStops(NearbyStop[] nearbyStops) {
        this.nearbyStops = nearbyStops;
    }

    /**
     * Define a listener that responds to location updates
     */
    LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            updateLocationOnUI(location);
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
            if (status == LocationProvider.AVAILABLE) {
                updateLocationOnUI();
            }
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };


    /**
     * Get last known location and then display it
     */
    private void updateLocationOnUI() {
        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
    }

    /**
     * Display date and location data
     */
    private void updateLocationOnUI(Location location) {
        if (location != null) {

            String locNow = (new Date()) + ", " + location.toString();
            Toast.makeText(getActivity(), locNow, Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onClick(View v) {

        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        new ObtainServiceTask().execute(currLoc);

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        // Click Item event
        if(this.nearbyStops != null){
            NearbyStop nearbyStop = nearbyStops[position];

            // Start new Activity
            Bundle bundle = new Bundle();
            bundle.putParcelable("currLoc", currLoc);
            Intent intent = new Intent(getActivity(), StopDetailActivity.class);
            intent.putExtra("stop", nearbyStop);
            intent.putExtra("currLoc", bundle);
            getActivity().startActivity(intent);

        }

    }

    private class ObtainServiceTask extends AsyncTask<Location, Void, Boolean> {

        @Override
        protected void onPreExecute() {

            progress.show();
        }



        @Override
        protected Boolean doInBackground(Location... params) {
//            Log.i(TAG, new PTService().healthCheck() ? "a" : "b");
            NearbyStop[] temp = new PTService().getServices(currLoc);
            // Filter Result
            nearbyStops = filterResult(temp);
//            Log.i(TAG, "doInBackGround, nearbyStops = " + nearbyStops.length);
            return nearbyStops != null && nearbyStops.length > 0;

        }



        @Override
        protected void onPostExecute(Boolean success) {

            if(success){
                showResult();
            }else{
                Toast.makeText(getActivity(), "Please Check Internet", Toast.LENGTH_SHORT).show();
            }
            // Close the Progress Dialog
            progress.dismiss();
        }
    }

    private NearbyStop[] filterResult(NearbyStop[] nearbyStops) {
        if(nearbyStops == null || nearbyStops.length == 0)
            return null;
        // Get the Pref
        SharedPreferences pref= getActivity().getSharedPreferences(Standard.PREF_FILE, 0);
        int setting = pref.getInt("setting", 0);

        Set<Entity> set = new HashSet<Entity>();
        List<Criteria> list = new ArrayList<Criteria>();

        if((setting & 0b1) == 0b1){
            // Train Selected
            list.add(new CriteriaTrain());
        }

        if((setting & 0b10) == 0b10){
            // Tram Selected
            list.add(new CriteriaTram());
        }

        if((setting & 0b100) == 0b100){
            // Bus Selected
            list.add(new CriteriaBus());
        }

        if((setting & 0b1000) == 0b1000){
            // V/Line Selected
            list.add(new CriteriaVLine());
        }else if((setting & 0b10000) == 0b10000){
            // NightRider Selected
            list.add(new CriteriaNightRider());
        }


        if(list.size() == 0) {
            return nearbyStops;
        }else{
            Set<Entity> set1 = new OrCriteria(list).meetCriteria(nearbyStops);
            NearbyStop[] ret = new NearbyStop[set1.size()];
            return new OrCriteria(list).meetCriteria(nearbyStops).toArray(ret);
        }
    }

    private void showResult() {

        // Prepare the list
        String[] names = NearbyStop.getStopNamesFromObj(this.nearbyStops);
        if(names != null)
        list.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, names));

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stops_nearby, container, false);
//
        // Register listener with Location Manager to receive location updates
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER,
                0, 0, locationListener);

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Getting Information");
        progress.setMessage("Please Wait....");
        progress.setIndeterminate(true);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);


        // ListActivity
        list = (ListView) rootView.findViewById(R.id.stop_nearby);
        list.setOnItemClickListener(this);

        // Get Current Location
        currLoc = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        new ObtainServiceTask().execute(currLoc);

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((MainActivity) activity).onSectionAttached(1);
    }




}
