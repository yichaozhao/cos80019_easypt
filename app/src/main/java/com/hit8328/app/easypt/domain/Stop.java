package com.hit8328.app.easypt.domain;

/**
 * Created by yichao on 17/11/14.
 */
public class Stop extends Entity{
    private String stop_id;

    private String distance;

    private String lon;

    private String location_name;

    private String transport_type;

    private String suburb;

    private String lat;

    public String getStop_id ()
    {
        return stop_id;
    }

    public void setStop_id (String stop_id)
    {
        this.stop_id = stop_id;
    }

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getLocation_name ()
    {
        return location_name;
    }

    public void setLocation_name (String location_name)
    {
        this.location_name = location_name;
    }

    public String getTransport_type ()
    {
        return transport_type;
    }

    public void setTransport_type (String transport_type)
    {
        this.transport_type = transport_type;
    }

    public String getSuburb ()
    {
        return suburb;
    }

    public void setSuburb (String suburb)
    {
        this.suburb = suburb;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString() {
        return "Stop{" +
                "stop_id='" + stop_id + '\'' +
                ", distance='" + distance + '\'' +
                ", lon='" + lon + '\'' +
                ", location_name='" + location_name + '\'' +
                ", transport_type='" + transport_type + '\'' +
                ", suburb='" + suburb + '\'' +
                ", lat='" + lat + '\'' +
                '}';
    }
}
