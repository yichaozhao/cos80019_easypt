package com.hit8328.app.easypt.serivce;

import android.util.Log;
import com.hit8328.app.easypt.domain.Entity;
import com.hit8328.app.easypt.domain.Record;

import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by yichao on 18/11/14.
 */
public class CriteriaExpired implements Criteria {
    private static final String TAG = "CriteriaExpired";

    @Override
    public Set<Entity> meetCriteria(Entity[] entities) {
        HashSet<Entity> newRecords = new HashSet<Entity>();
        Log.i(TAG, "Record Length = " + entities.length);

        for(Entity record: entities){

//            Record record = iter.next();
            if(((Record)record).getTimeArrival().compareTo(new Date()) > 0){
                newRecords.add(record);
            }

        }

        Log.i(TAG, "meetCriteria records number " + newRecords);
        return newRecords;
    }
}
